#pragma once

#include<iostream>
#include "cSchienenfahrzeug.h"

using namespace std;

class cLokomotive :
	public cSchienenfahrzeug
{
private:
	double triebkraft;
public:
	cLokomotive(double = 0.0, double = 0.0);
	void ankuppeln();
	double bereitstellen(double = 0.0);
};

