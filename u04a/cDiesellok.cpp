#include "cDiesellok.h"

// Konstruktor
cDiesellok::cDiesellok(double trieb_in, double gew_in) : cLokomotive(trieb_in, gew_in) {
}

// Treibstoff tanken und Power berechnen
double cDiesellok::bereitstellen(double treibstoff) {
	double sumTreibstoff = treibstoff + tanken();
	return cLokomotive::bereitstellen(sumTreibstoff);
}

double cDiesellok::tanken() {
	return 2000.0;
}