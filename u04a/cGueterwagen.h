#pragma once
#include "cWaggon.h"
class cGueterwagen :
    public cWaggon
{
private:
    double nutzlast;
public:
    cGueterwagen(double = 80000.0, double = 18500.0);
    double zuladen(double);
    double abladen(double);
    double get_gewicht();
};

