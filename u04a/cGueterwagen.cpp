#include "cGueterwagen.h"

// Konstruktor, Vererbung von Waggon
cGueterwagen::cGueterwagen(double nutz_in, double gew_in) : cWaggon(gew_in) {
	nutzlast = nutz_in;
}

// Last aufladen
double cGueterwagen::zuladen(double lastplus) {
	nutzlast += lastplus;
	return nutzlast;
}

// Last abladen
double cGueterwagen::abladen(double lastweg) {
	nutzlast -= lastweg;
	return nutzlast;
}

// get_gewicht overwrite
double cGueterwagen::get_gewicht() {
	return cSchienenfahrzeug::get_gewicht() + nutzlast;
}