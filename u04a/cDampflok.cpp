#include "cDampflok.h"

// Konstruktor
cDampflok::cDampflok(double trieb_in, double gew_in) : cLokomotive(trieb_in, gew_in) {
}

// Kohle einwerfen und Power berechnen
double cDampflok::bereitstellen(double kohle) {
	double calcEnergy = (kohle - anheizen()) * 0.75;
	return cLokomotive::bereitstellen(calcEnergy);
}

double cDampflok::anheizen() {
	return 300.0;
}