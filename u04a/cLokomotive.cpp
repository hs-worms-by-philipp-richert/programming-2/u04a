#include "cLokomotive.h"

// Konstruktor
cLokomotive::cLokomotive(double triebkraft_in, double gewicht_in) : cSchienenfahrzeug(gewicht_in) {
	triebkraft = triebkraft_in;
}

// Ankuppeln an Waggons
void cLokomotive::ankuppeln() {
	cout << "Ich zieh Euch alle" << endl;
}

// Power berechnen
double cLokomotive::bereitstellen(double treibstoff) {
	return ((cSchienenfahrzeug::get_gewicht() + treibstoff) * triebkraft) / 1000.0;
}