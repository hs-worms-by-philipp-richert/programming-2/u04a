#include "cPersonenwagen.h"

// Konstruktor
cPersonenwagen::cPersonenwagen(int fahrg_in, int gew_in) : cWaggon(gew_in) {
	fahrgastzahl = fahrg_in;
}

// Addition auf fahrgastzahl
int cPersonenwagen::einsteigen(int rein) {
	int sumFahrg = fahrgastzahl + rein;
	fahrgastzahl = (sumFahrg > 117) ? 117 : sumFahrg;

	return fahrgastzahl;
}

// Subtraktion von fahrgastzahl
int cPersonenwagen::aussteigen(int raus) {
	int newFahrg = fahrgastzahl - raus;
	fahrgastzahl = (newFahrg < 0) ? 0 : newFahrg;

	return fahrgastzahl;
}