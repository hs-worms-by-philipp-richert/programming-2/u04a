#pragma once
#include "cWaggon.h"
class cPersonenwagen :
    public cWaggon
{
private:
    int fahrgastzahl;
public:
    cPersonenwagen(int = 12, int = 27300.0);
    int einsteigen(int);
    int aussteigen(int);
};

