#pragma once
#include "cLokomotive.h"
class cDampflok :
    public cLokomotive
{
private:
    double anheizen();
public:
    cDampflok(double = 2250.0, double = 120000.0);
    double bereitstellen(double);
};

