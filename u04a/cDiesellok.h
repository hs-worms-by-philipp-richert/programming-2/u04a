#pragma once
#include "cLokomotive.h"
class cDiesellok :
    public cLokomotive
{
private:
    double tanken();
public:
    cDiesellok(double = 4800.0, double = 87000.0);
    double bereitstellen(double);
};

